#!/usr/bin/env bash

#DATASET_PATH="/home/risen/datasets-nas/cifar100"
DATASET_PATH="/media/mint/Barracuda/Datasets/cifar10"
DATASET_NAME='cifar10'
GPU="0"
MODEL="resnet18"
BATCH_SIZE="1024"

mkdir -p "logs/multi_optimizer/${MODEL}"
for RUN_NUMBER in 1 2 3 4 5 6 7
do
  python3 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr=0.008 --batch-size="$BATCH_SIZE" \
          --momentum=0.95 --adam-w=0.3 --sgd-w=0.7 --gpu="$GPU"\
          --model="$MODEL" > "logs/multi_optimizer/${DATASET_NAME}/${MODEL}/weighted_0.3_0.7_momentum0.95_$RUN_NUMBER.log"
done