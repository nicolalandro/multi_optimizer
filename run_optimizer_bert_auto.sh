#!/usr/bin/env bash

#DATASET_PATH="/media/mint/Barracuda/Datasets/cola_public/raw"
DATASET_PATH="/home/super/datasets/cola_public/raw"
DATASET_NAME='cola'
#DATASET_PATH="/media/mint/Barracuda/Datasets/AGnews"
#DATASET_PATH="/home/super/datasets/AGnews"
#DATASET_NAME='agnews'
MODEL_NAME='BERT'
GPU="1"
BATCH_SIZE="100"
LR=0.0002
MOMENTUM=0.95
EPOCHS=50

MAS_CHANGE="no"
ADAM_END_W=0.0
ADAM_W=0.2
SGD_W=0.8

source venv/bin/activate

for RUN_NUMBER in 4 5 6
do
  CUDA_VISIBLE_DEVICES="$GPU" python3 src/bert_train.py \
        --path="$DATASET_PATH" --dataset="${DATASET_NAME}" --lr="$LR" --batch-size="$BATCH_SIZE" \
        --momentum="$MOMENTUM" --adam-w="$ADAM_W" --sgd-w="$SGD_W" --gpu="$GPU" --max-epoch="$EPOCHS" \
        --seed="$RUN_NUMBER" \
        --mas-change="${MAS_CHANGE}" --adam-end-w="${ADAM_END_W}" \
        > "logs/multi_optimizer/${DATASET_NAME}/${MODEL_NAME}/n1_w_${ADAM_W}_${SGD_W}_mom_${MOMENTUM}_lr_${LR}_mas_change_${MAS_CHANGE}_adam_w_end_${ADAM_END_W}_${RUN_NUMBER}.log"
done
