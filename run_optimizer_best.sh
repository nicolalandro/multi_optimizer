#!/usr/bin/env bash

#DATASET_PATH="/home/risen/datasets-nas/cifar100"
DATASET_PATH="/media/mint/Barracuda/Datasets/cifar10"
DATASET_NAME='cifar10'
GPU="0"
MODEL="resnet18"
BATCH_SIZE="1024"
RUN_NUMBER=1
LR=0.1


mkdir -p "logs/multi_optimizer/${MODEL}"

ADAM_W=0.5
SGD_W=0.5
python3 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr=0.008 --batch-size="$BATCH_SIZE" \
          --momentum=0.95 --adam-w="$ADAM_W" --sgd-w="$SGD_W" --gpu="$GPU" --lr-change="cosineannealinglr"\
          --model="$MODEL" > "logs/multi_optimizer/${DATASET_NAME}/${MODEL}/weighted_${ADAM_W}_${SGD_W}_momentum0.95_lr_${LR}_${RUN_NUMBER}.log"

ADAM_W=1
SGD_W=0
python3 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr=0.008 --batch-size="$BATCH_SIZE" \
          --momentum=0.95 --adam-w="$ADAM_W" --sgd-w="$SGD_W" --gpu="$GPU" --lr-change="cosineannealinglr"\
          --model="$MODEL" > "logs/multi_optimizer/${DATASET_NAME}/${MODEL}/weighted_${ADAM_W}_${SGD_W}_momentum0.95_lr_${LR}_${RUN_NUMBER}.log"

ADAM_W=0
SGD_W=1
python3 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr=0.008 --batch-size="$BATCH_SIZE" \
          --momentum=0.95 --adam-w="$ADAM_W" --sgd-w="$SGD_W" --gpu="$GPU" --lr-change="cosineannealinglr"\
          --model="$MODEL" > "logs/multi_optimizer/${DATASET_NAME}/${MODEL}/weighted_${ADAM_W}_${SGD_W}_momentum0.95_lr_${LR}_${RUN_NUMBER}.log"
