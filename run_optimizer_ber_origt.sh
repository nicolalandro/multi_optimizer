#!/usr/bin/env bash

#DATASET_PATH="/media/mint/Barracuda/Datasets/cola_public/raw"
#DATASET_PATH="/home/super/datasets/cola_public/raw"
#DATASET_NAME='cola'
#DATASET_PATH="/media/mint/Barracuda/Datasets/AGnews"
DATASET_PATH="/home/super/datasets/AGnews"
DATASET_NAME='agnews'
MODEL_NAME='BERT'
GPU="0"
BATCH_SIZE="100"
LR=0.0002
MOMENTUM=0.95
EPOCHS=10

source venv/bin/activate

for RUN_NUMBER in 1 2 3 4 5
do
  CUDA_VISIBLE_DEVICES="$GPU" python3 src/bert_train.py --path="$DATASET_PATH" --dataset="${DATASET_NAME}" --lr="$LR" --batch-size="$BATCH_SIZE" \
        --momentum="$MOMENTUM" --gpu="$GPU" --max-epoch="$EPOCHS" --seed="$RUN_NUMBER" --original="1" \
        > "logs/multi_optimizer/${DATASET_NAME}/${MODEL_NAME}/original_${RUN_NUMBER}.log"
done
