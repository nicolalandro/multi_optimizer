# coding: utf-8
# from https://github.com/yuxwhkbu/ptb_lstm_torch
import argparse
import time
import math
import os
import torch
import torch.nn as nn
import logging
from src.datasets import data
from src.models import rnn_model as model
from src.optimizers.adam_sgd_mix import AdamSGDWeighted

parser = argparse.ArgumentParser(description='LSTM PTB Language Model')
parser.add_argument('--data', type=str, default='/media/mint/Barracuda/Datasets/ptbdataset',
                    help='location of the data corpus')
parser.add_argument('--model', type=str, default='LSTM',
                    help='type of recurrent net (LSTM, GRU)')
parser.add_argument('--embedding_size', type=int, default=200,
                    help='size of word embeddings')
parser.add_argument('--num_hid_unit', type=int, default=200,
                    help='number of hidden units per layer')
parser.add_argument('--nlayers', type=int, default=2,
                    help='number of layers')
parser.add_argument('--lr', type=float, default=0.2,
                    help='initial learning rate')
# new
parser.add_argument('--momentum', type=float, default=0.95, help="sgd momentum")
parser.add_argument('--adam-w', type=float, default=0.3, help="adam weight")
parser.add_argument('--sgd-w', type=float, default=0.7, help="sgd weight")

parser.add_argument('--clip', type=float, default=0.25,
                    help='gradient clipping')
parser.add_argument('--epochs', type=int, default=40,
                    help='upper epoch limit')
parser.add_argument('--batch_size', type=int, default=2000, metavar='N',
                    help='batch size')
parser.add_argument('--bptt', type=int, default=35,
                    help='sequence length')
parser.add_argument('--dropout', type=float, default=0.2,
                    help='dropout applied to layers (0 = no dropout)')
parser.add_argument('--tied', action='store_true',
                    help='tie the word embedding and softmax weights')
parser.add_argument('--seed', type=int, default=1111,
                    help='random seed')
parser.add_argument('--seed_gpu', type=int, default=1111,
                    help='random seed')
parser.add_argument('--cuda', action='store_true',
                    help='use CUDA')
parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

parser.add_argument('--log-interval', type=int, default=1, metavar='N',
                    help='report interval')
parser.add_argument('--save', type=str, default='/media/mint/Barracuda/Models/PTB/LSTM/model.pt',
                    help='path to save the final model')

parser.add_argument('--base', type=int, default=0,
                    help="if not 0 use the original code, otherwise use the new optimizer")

args = parser.parse_args()

torch.manual_seed(args.seed)
torch.cuda.manual_seed(args.seed_gpu)

device = torch.device("cuda" if args.cuda else "cpu")


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
        return 'cuda'
    else:
        print("Currently using CPU")
        return 'cpu'


test_gpu_settings()


def batchify(data, bsz):
    nbatch = data.size(0) // bsz
    data = data.narrow(0, 0, nbatch * bsz)
    data = data.view(bsz, -1).t().contiguous()
    return data.to(device)


corpus = data.Corpus(args.data)

eval_batch_size = 10
train_data = batchify(corpus.train, args.batch_size)
val_data = batchify(corpus.valid, eval_batch_size)
test_data = batchify(corpus.test, eval_batch_size)

ntokens = len(corpus.dictionary)
model = model.RNNModel(args.model, ntokens, args.embedding_size, args.num_hid_unit, args.nlayers, args.dropout,
                       args.tied).to(device)

criterion = nn.CrossEntropyLoss().to(device)

optimizer = AdamSGDWeighted(model.parameters(), args.lr, momentum=args.momentum, adam_w=args.adam_w, sgd_w=args.sgd_w)


# optimizer = torch.optim.SGD(model.parameters(), lr=args.lr)

def get_batch(source, i):
    seq_len = min(args.bptt, len(source) - 1 - i)
    data = source[i:i + seq_len]
    target = source[i + 1:i + 1 + seq_len].view(-1)
    return data, target


def evaluate(data_source):
    model.eval()
    total_loss = 0.
    ntokens = len(corpus.dictionary)
    hidden = model.init_hidden(eval_batch_size)
    with torch.no_grad():
        for i in range(0, data_source.size(0) - 1, args.bptt):
            data, targets = get_batch(data_source, i)
            output, hidden = model(data, hidden)
            output_flat = output.view(-1, ntokens)
            total_loss += len(data) * criterion(output_flat, targets).item()
            hidden = repackage_hidden(hidden)
    return total_loss / (len(data_source) - 1)


def repackage_hidden(h):
    """Wraps hidden states in new Tensors, to detach them from their history."""
    if isinstance(h, torch.Tensor):
        return h.detach()
    else:
        return tuple(repackage_hidden(v) for v in h)


def initLogging(logFilename):
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s-%(levelname)s-%(message)s',
        datefmt='%y-%m-%d %H:%M',
        filename=logFilename,
        filemode='w');
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)s-%(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)


initLogging('../logs/multi_optimizer/ptb/test.log')


def train():
    model.train()
    total_loss = 0.
    start_time = time.time()
    ntokens = len(corpus.dictionary)
    hidden = model.init_hidden(args.batch_size)
    for batch, i in enumerate(range(0, train_data.size(0) - 1, args.bptt)):
        data, targets = get_batch(train_data, i)
        hidden = repackage_hidden(hidden)

        # optimizer = torch.optim.SGD(model.parameters(), lr=args.lr)
        optimizer.zero_grad()
        output, hidden = model(data, hidden)
        loss = criterion(output.view(-1, ntokens), targets)
        loss.backward()

        if args.base == 0:
            optimizer.step()
        else:
            with torch.no_grad():
                torch.nn.utils.clip_grad_norm_(model.parameters(), args.clip)
                for p in model.parameters():
                    p.add_(p.grad, alpha=-lr)

        total_loss += loss.item()

        if batch % args.log_interval == 0 and batch > 0:
            cur_loss = total_loss / args.log_interval
            elapsed = time.time() - start_time
            logging.info('| epoch {:3d} | {:5d}/{:5d} batches | lr {:02.2f} | ms/batch {:5.2f} | '
                         'loss {:5.2f} | ppl {:8.2f}'.format(
                epoch, batch, len(train_data) // args.bptt, lr,
                              elapsed * 1000 / args.log_interval, cur_loss, math.exp(cur_loss)))
            total_loss = 0
            start_time = time.time()


lr = args.lr
best_val_loss = None

try:
    for epoch in range(1, args.epochs + 1):
        print('Sart Epoch: ', epoch)
        epoch_start_time = time.time()
        train()
        print('  end train')
        val_loss = evaluate(val_data)
        print('  end val')
        test_loss = evaluate(test_data)
        print('  end test')
        logging.info('-' * 89)
        logging.info('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | test loss {:5.2f} | '
                     .format(epoch, (
                time.time() - epoch_start_time),
                             val_loss,
                             test_loss))
        logging.info('-' * 89)
        if not best_val_loss or val_loss < best_val_loss:
            with open(args.save, 'wb') as f:
                torch.save(model, f)
            best_val_loss = val_loss
        else:
            lr /= 2.5
except KeyboardInterrupt:
    logging.info('-' * 89)
    logging.info('Exiting from training early')

with open(args.save, 'rb') as f:
    model = torch.load(f)

    model.rnn.flatten_parameters()

test_loss = evaluate(test_data)
logging.info('=' * 89)
logging.info('| End of training | test loss {:5.2f} | test ppl {:8.2f}'.format(
    test_loss, math.exp(test_loss)))
logging.info('=' * 89)
