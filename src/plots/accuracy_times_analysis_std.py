import os

import matplotlib.pyplot as plt
import numpy as np

from src.plots.multi_optimizer_accuracy_plot_from_file import get_accuracies

# log_start_with = 'sgd_momentum'
log_start_with = 'adam'
# log_start_with = 'weighted_0.5_0.5'
# log_start_with = 'w_0.0_1.0'  # SGD
# log_start_with = 'w_1.0_0.0' # ADAM
# log_start_with = 'original'
# log_start_with = 'w_0.5_0.5'
model = 'resnet18'
dataset = 'cifar10'
# model = 'BERT'
# dataset = 'agnews'
log_path = f'../../logs/multi_optimizer/{dataset}/{model}/'
output_path = f'../../images/multi_optimizer/{dataset}/{model}/{log_start_with}_std.pdf'

every_epochs_mean = 10


def main():
    log_files = os.listdir(log_path)
    log_files = list(filter(lambda x: x.startswith(log_start_with), log_files))
    all_epochs_time = []
    for f_name in log_files:
        f_path = os.path.join(log_path, f_name)
        epochs, accuracies, _ = get_accuracies(f_path, verbose=False)
        accuracies = accuracies[0]
        print(f_name, sum(accuracies) / len(accuracies))
        all_epochs_time.append(accuracies)

    epochs = np.mean(np.array(epochs).reshape(-1, every_epochs_mean), axis=1)
    for i, t in enumerate(all_epochs_time):
        t = np.array(t)
        all_epochs_time[i] = np.mean(t.reshape(-1, every_epochs_mean), axis=1)
        print(len(all_epochs_time[i]))

    # print('\nTotal avg', sum(all_epochs_time) / len(all_epochs_time))
    ac_min = []
    ac_max = []
    ac_mean = []
    ac_std = []
    for acc in zip(*all_epochs_time):
        acc = np.array(acc)
        ac_min.append(acc.min(0))
        ac_max.append(acc.max(0))
        ac_mean.append(acc.mean(0))
        ac_std.append((acc.std(0)))

    ac_min = np.array(ac_min)
    ac_max = np.array(ac_max)
    ac_mean = np.array(ac_mean)
    ac_std = np.array(ac_std)
    plt.figure(figsize=(3, 1))
    f, (ax, axleg) = plt.subplots(2, figsize=(10, 10))

    ax.errorbar(epochs, ac_mean, ac_std, fmt='ok', lw=3, label=log_start_with, ecolor='blue')
    ax.errorbar(epochs, ac_mean, [ac_mean - ac_min, ac_max - ac_mean],
                fmt='.k', ecolor='blue', lw=1)

    ax.set_ylabel('Test Accuracy')
    ax.set_xlabel('Epoch')
    axleg.axis('off')
    ax.legend()
    # plt.show()
    plt.savefig(output_path)


if __name__ == '__main__':
    main()
