import os

import matplotlib.pyplot as plt
import numpy as np

from src.plots.multi_optimizer_accuracy_plot_from_file import get_accuracies

# log_start_with = 'sgd_momentum'
# log_start_with = 'adam'
log_start_withs = ['artstate_weighted_0.0_1.0', 'artstate_weighted_1.0_0.0', 'artstate_weighted_1.0_0.0']
log_contains = ['mas_change_no', 'mas_change_no', 'mas_change_step']
# log_start_with = 'w_0.0_1.0'  # SGD
# log_start_with = 'w_1.0_0.0' # ADAM
# log_start_with = 'original'
# log_start_with = 'w_0.5_0.5'
model = 'resnet18'
dataset = 'cifar10'
# model = 'BERT'
# dataset = 'agnews'
log_path = f'../../logs/multi_optimizer/{dataset}/{model}/'
every_epochs_mean = 35
output_path = f'../../images/multi_optimizer/{dataset}/{model}/ATMO_ADAM_SGD_bar_{every_epochs_mean}.pdf'


def main():
    plt.figure(figsize=(3, 1))
    f, (ax, axleg) = plt.subplots(2, figsize=(10, 10))

    ac_max, ac_mean, ac_min, epochs = plot_for_all_group(log_start_withs[0], log_contains[0])

    X_axis = np.arange(len(epochs))
    ax.set_xticks(X_axis)
    ax.set_xticklabels([str(int(e * every_epochs_mean + every_epochs_mean)) for e in X_axis])

    ax.bar(X_axis - 0.2, ac_max, 0.2, label='SGD max', color=[0, 209/255, 0])
    ax.bar(X_axis - 0.2, ac_mean, 0.2, label='SGD avg', color=[138/255, 255/255, 138/255])
    ax.bar(X_axis - 0.2, ac_min, 0.2, label='SGD min', color=[204/255, 255/255, 204/255])

    ac_max, ac_mean, ac_min, _ = plot_for_all_group(log_start_withs[1], log_contains[1])
    ax.bar(X_axis, ac_max, 0.2, label='ADAM max', color=[209/255, 0, 0])
    ax.bar(X_axis, ac_mean, 0.2, label='ADAM avg', color=[255/255, 138/255, 138/255])
    ax.bar(X_axis, ac_min, 0.2, label='ADAM min', color=[255/255, 204/255, 204/255])

    ac_max, ac_mean, ac_min, _ = plot_for_all_group(log_start_withs[2], log_contains[2])
    ax.bar(X_axis + 0.2, ac_max, 0.2, label='ATMO max', color=[0, 0, 209/255])
    ax.bar(X_axis + 0.2, ac_mean, 0.2, label='ATMO avg', color=[138/255, 138/255, 255/255])
    ax.bar(X_axis + 0.2, ac_min, 0.2, label='ATMO min', color=[204/255, 204/255, 255/255])

    ax.set_ylabel('Test Accuracy')
    ax.set_xlabel('Epoch')
    axleg.axis('off')
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.3), shadow=True, ncol=1)
    ax.set_ylim([.65, .95])
    # plt.show()
    plt.savefig(output_path)


def plot_for_all_group(log_start_with, log_contains):
    log_files = os.listdir(log_path)
    log_files = list(filter(lambda x: x.startswith(log_start_with), log_files))
    log_files = list(filter(lambda x: log_contains in x, log_files))
    print('Files:', len(log_files))

    all_epochs_time = []
    for f_name in log_files:
        f_path = os.path.join(log_path, f_name)
        epochs, accuracies, _ = get_accuracies(f_path, verbose=False)
        accuracies = accuracies[0]
        print(f_name, sum(accuracies) / len(accuracies))
        if len(accuracies) < 350:
            print('\t SKIPPED')
            continue
        # if len(accuracies) > 200:
        #     accuracies = accuracies[:200]
        #     epochs = epochs[:200]
        all_epochs_time.append(accuracies)
    epochs = np.mean(np.array(epochs).reshape(-1, every_epochs_mean), axis=1)
    for i, t in enumerate(all_epochs_time):
        t = np.array(t)
        all_epochs_time[i] = np.mean(t.reshape(-1, every_epochs_mean), axis=1)
    # print('\nTotal avg', sum(all_epochs_time) / len(all_epochs_time))
    ac_min = []
    ac_max = []
    ac_mean = []
    ac_std = []
    for acc in zip(*all_epochs_time):
        acc = np.array(acc)
        ac_min.append(acc.min(0))
        ac_max.append(acc.max(0))
        ac_mean.append(acc.mean(0))
        ac_std.append((acc.std(0)))
    ac_min = np.array(ac_min)
    ac_max = np.array(ac_max)
    ac_mean = np.array(ac_mean)
    return ac_max, ac_mean, ac_min, epochs


if __name__ == '__main__':
    main()
