import os

from src.plots.multi_optimizer_accuracy_plot_from_file import get_accuracies

dataset = 'cifar10'
model = 'resnet34'
folder_path = f'../../logs/new_multi_optimizer/{dataset}/{model}/'

files = os.listdir(folder_path)

all_best_accuracies = []

files.sort()
old_f = 'none'
for f in files:
    try:
        clean_f = f[:-6] if f[:-4].endswith('_1') else f[:-4]
        log_path = os.path.join(folder_path, f)
        epochs, opt_accuracies, opt_names = get_accuracies(log_path, verbose=0)

        if not clean_f.startswith(old_f):
            old_f = clean_f
            all_best_accuracies.append([])
            index = len(all_best_accuracies) - 1

        flat_list = [item for sublist in opt_accuracies for item in sublist]
        best_acc = max(flat_list)
        all_best_accuracies[index].append((f, best_acc))
    except Exception as e:
        print('ERROR:', f, e)

all_best_accuracies_mean = []
for r in all_best_accuracies:
    runs = len(r)
    if runs > 0:
        mean_acc = sum([x[1] for x in r]) / runs
        max_acc = max([x[1] for x in r])
        name = r[0][0][:-4]
        all_best_accuracies_mean.append((name, mean_acc, max_acc, runs))

all_best_accuracies_mean.sort(key=lambda tup: tup[1], reverse=True)
for i, (n, a, m, r) in enumerate(all_best_accuracies_mean):
    print(f'{i})', n, f'({r} runs)', 'mean:', a, 'max:', m)
