import matplotlib.pyplot as plt
import numpy as np
import torch
from numpy.ma import arange
from scipy.interpolate import griddata
from scipy.ndimage import gaussian_filter
from torch.optim import SGD, Adam

from src.optimizers.adam_sgd_mix import AdamSGDWeighted


def f(w1, w2):
    x = torch.tensor([[1.], [2.]])
    y = torch.tensor([[2.], [4.]])
    pred = w2 * (w1 * x)
    loss = torch.sum((y - pred) ** 2)
    return loss


def f_a(w1, w2):
    return torch.abs(w1) + torch.abs(w2)


def f_b(w1, w2):
    return torch.abs(w1 + w2) + torch.abs(w1 - w2)


def f_c(w1, w2):
    return (w1 + w2) ** 2 + (w1 - w2) ** 2 / 10


def f_d(w1, w2):
    return torch.abs(w1) / 10 + torch.abs(w2)


# Beale
def f_e(w1, w2):
    return (torch.tensor(1.5) - w1 + w1 * w2) ** 2 \
           + (torch.tensor(2.5) - w1 + w1 * (w2 ** 2)) ** 2 \
           + (torch.tensor(2.625) - w1 + w1 * (w2 ** 3)) ** 2


# Rosenbrock
def f_g(w1, w2):
    # minimum in (a, a**2)
    a = torch.tensor(1.)
    b = torch.tensor(100.)
    return (a - w1) ** 2 + b * (w2 - w1 ** 2) ** 2


minimums = {
    str(f_a): (0, 0, 0),
    str(f_b): (0, 0, 0),
    str(f_c): (0, 0, 0),
    str(f_d): (0, 0, 0),
    str(f_e): (0, 0, 0),
    str(f_g): (1, 1, 0)
}


def testOptimizer(optimizer, w1, w2, epochs=10, fu=f):
    points_x = []
    points_y = []
    points_z = []
    for epoch in range(epochs):
        loss = fu(w1, w2)
        # print(w1, w2, loss)

        points_x.append(w1.item())
        points_y.append(w2.item())
        points_z.append(loss.item())

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    return points_x, points_y, points_z


if __name__ == '__main__':
    plt.rcParams.update({'font.size': 22})
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    fig2 = plt.figure()
    ax2 = fig2.gca()
    linewidth = 2

    min_x, max_x, min_y, max_y = 4, 0, 4, 0
    fu = f
    lr = 0.0001
    epochs = 1000
    if str(fu) != str(f):
        x, y, z = minimums[str(fu)]
        ax.scatter(x, y, z, marker='x', c='black')
        ax2.scatter(x, y, marker='x', c='black')
    if str(fu) == str(f_d):
        lr = 0.01
        epochs = 400
    base_w1 = 3.
    base_w2 = 2.
    if str(fu) == str(f):
        base_w1 = 4.
        base_w2 = 4.
    elif str(fu) == str(f_g):
        base_w2 = 1.

    sgd_color = [1, 0, 0]
    adam_color = [0, 0, 1]
    mas_color = [0, 1, 0]

    w1 = torch.tensor(base_w1, requires_grad=True)
    w2 = torch.tensor(base_w2, requires_grad=True)
    if str(fu) == str(f):
        lr = 0.002
        epochs = 100
    sgd = SGD(params=[w1, w2], lr=lr)
    # sgd = AdamSGDWeighted(params=[w1, w2], lr=lr, adam_w=0.0, sgd_w=1.0)
    points_x, points_y, points_z = testOptimizer(sgd, w1, w2, epochs=epochs, fu=fu)
    # shift to do better visualization
    if str(fu) == str(f):
        points_x = [x - 0.1 for x in points_x]
    ax.scatter(points_x, points_y, points_z, c=sgd_color, s=10, antialiased=True)
    ax.plot(points_x, points_y, points_z, c=sgd_color, antialiased=True, label='SGD')
    ax2.plot(points_x, points_y, c=sgd_color, antialiased=True, label='SGD', linewidth=linewidth)
    min_x = min(min_x, min(points_x))
    max_x = max(max_x, max(points_x))
    min_y = min(min_y, min(points_y))
    max_y = max(max_y, max(points_y))
    # print(min_x, max_x, min_y, max_y)

    w1 = torch.tensor(base_w1, requires_grad=True)
    w2 = torch.tensor(base_w2, requires_grad=True)
    if str(fu) == str(f):
        lr = 0.2
        epochs = 25
    adam = Adam(params=[w1, w2], lr=lr)
    # adam = AdamSGDWeighted(params=[w1, w2], lr=lr * 100, adam_w=1.0, sgd_w=0.0)
    points_x, points_y, points_z = testOptimizer(adam, w1, w2, epochs=epochs, fu=fu)
    ax.scatter(points_x, points_y, points_z, c=adam_color, s=10, antialiased=True)
    ax.plot(points_x, points_y, points_z, c=adam_color, antialiased=True, label='ADAM')
    ax2.plot(points_x, points_y, c=adam_color, antialiased=True, label='ADAM', linewidth=linewidth)
    min_x = min(min_x, min(points_x))
    max_x = max(max_x, max(points_x))
    min_y = min(min_y, min(points_y))
    max_y = max(max_y, max(points_y))
    # print(min_x, max_x, min_y, max_y)

    w1 = torch.tensor(base_w1, requires_grad=True)
    w2 = torch.tensor(base_w2, requires_grad=True)
    if str(fu) == str(f):
        lr = 0.002
        epochs = 3
    mas = AdamSGDWeighted(params=[w1, w2], lr=lr, adam_w=0.5, sgd_w=0.5)
    points_x, points_y, points_z = testOptimizer(mas, w1, w2, epochs=epochs, fu=fu)
    ax.scatter(points_x, points_y, points_z, c=mas_color, s=10, antialiased=True)
    ax.plot(points_x, points_y, points_z, c=mas_color, antialiased=True, label='ATMO')
    ax2.plot(points_x, points_y, c=mas_color, antialiased=True, label='ATMO', linewidth=linewidth)
    min_x = min(min_x, min(points_x))
    max_x = max(max_x, max(points_x))
    min_y = min(min_y, min(points_y))
    max_y = max(max_y, max(points_y))
    # print(min_x, max_x, min_y, max_y)

    x = []
    y = []
    z = []
    pad = 0
    resolution = 20
    min_x = min_x - pad
    min_y = min_y - pad
    x_step = (max_x - min_x) / resolution
    y_step = (max_y - min_y) / resolution
    for w1_val in arange(min_x - x_step, max_x + x_step, x_step):
        for w2_val in arange(min_y - y_step, max_y + y_step, y_step):
            w1_tens = torch.tensor(w1_val)
            x.append(w1_tens.item())

            w2_tens = torch.tensor(w2_val)
            y.append(w2_tens.item())

            loss = fu(w1_tens, w2_tens)
            z.append(loss.item())

    # for color maps https://matplotlib.org/3.1.1/tutorials/colors/colormaps.html
    ax.plot_trisurf(x, y, z, linewidth=0.2, alpha=0.5, cmap='viridis')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    if str(fu) == str(f):
        ax.legend()
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_zticklabels([])

    # plt.show()
    # fig.margins(0, 0, 0)
    if str(fu) == str(f_g):
        angle = 360 / 2
        ax.view_init(30, angle)

    fig.savefig('../../images/toy.pdf', bbox_inches='tight', pad_inches=0)

    xi = np.linspace(min(x), max(x), 100)
    yi = np.linspace(min(y), max(y), 100)
    X, Y = np.meshgrid(xi, yi)
    Z = griddata((x, y), z, (X, Y), method='nearest')
    Z = gaussian_filter(Z, sigma=15)
    # ax2.contourf(X, Y, Z, alpha=0.8, cmap='seismic')
    ax2.pcolormesh(X, Y, Z, alpha=0.6, shading='auto', cmap='viridis')
    ax2.set_xlabel('X')
    ax2.set_ylabel('Y')
    ax2.legend(loc='lower right', shadow=True, ncol=1)

    # plt.show()
