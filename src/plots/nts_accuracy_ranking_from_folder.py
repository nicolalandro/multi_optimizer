import os


def get_accuracies(log_path, verbose=True, limit=-1):
    with open(log_path, "r") as f:
        logs = str(f.readlines())
    tests = logs.split('epoch:')[1:][:limit]
    accuracies = []
    epochs = []
    for t in tests:
        epoch = t.split(' ')[0]
        epochs.append(int(epoch))
        acc = t.split('test acc: ')[1].split(' ')[0].strip()
        if verbose:
            print(acc)
        accuracies.append(float(acc))
    return epochs, accuracies


dataset = 'cub200'
model = 'ntsnet'
folder_path = f'../../logs/multi_optimizer/{dataset}/{model}/'

files = os.listdir(folder_path)

all_best_accuracies = []

for f in files:
    log_path = os.path.join(folder_path, f)

    epochs, accuracies = get_accuracies(log_path, verbose=False, limit=49)
    best_acc = max(accuracies)
    max_index = accuracies.index(best_acc)
    best_epoch = epochs[max_index]
    all_best_accuracies.append((f, best_epoch, max(epochs), best_acc))

all_best_accuracies.sort(key=lambda tup: tup[3], reverse=True)
for i, (n, e, me, a) in enumerate(all_best_accuracies):
    print(f'{i})', n, '-->', a, 'at epoch', f'{e}/{me}')
