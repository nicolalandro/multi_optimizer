import os
import re

from src.plots.bert_accuracy_ranking_from_folder import get_accuracies

dataset = 'cola'
model = 'BERT'
folder_path = f'../../logs/multi_optimizer/{dataset}/{model}/'

files = os.listdir(folder_path)
# files = list(filter(lambda x: x.startswith('n'), files))
files.sort()

all_best_accuracies = []

old_f = 'none'

if dataset == 'cola':
    ep = 49
elif dataset == 'agnews':
    ep = 9
for f in files:
    clean_f = f[:-6] if re.search(r'_\d$', f[:-4]) else f[:-4]
    if not clean_f.startswith(old_f):
        old_f = clean_f
        all_best_accuracies.append([])
        index = len(all_best_accuracies) - 1
    log_path = os.path.join(folder_path, f)

    try:
        epochs, opt_accuracies = get_accuracies(log_path, verbose=0)
        if len(epochs) != ep:
            raise Exception(f'epochs are {len(epochs)} instead of {ep}')
        flat_list = opt_accuracies
        best_acc = max(flat_list)
        all_best_accuracies[index].append((f, best_acc))
    except Exception as e:
        print(log_path, e)

all_best_accuracies_mean = []
for r in all_best_accuracies:
    runs = len(r)
    if runs > 0:
        mean_acc = sum([x[1] for x in r]) / runs
        max_max = max([x[1] for x in r])
        name = r[0][0][:-4]
        all_best_accuracies_mean.append((name, mean_acc, max_max, runs))

all_best_accuracies_mean.sort(key=lambda tup: tup[1], reverse=True)
for i, (n, a, m, r) in enumerate(all_best_accuracies_mean):
    print(f'{i})', n, f'({r} runs)', '--> mean:', a, ', max:', m)
