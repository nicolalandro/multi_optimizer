import matplotlib.pyplot as plt

file_name = 'window10_xent_jaccard_onehotmse'
model = 'lenet5'
dataset = 'cifar10'
log_path = f'../../logs/multi_loss/{dataset}/{model}/{file_name}.log'
output_path = f'../../images/multi_loss/{dataset}/{model}/{file_name}.pdf'


def main():
    epochs, opt_accuracies, opt_names = get_accuracies(log_path)

    plt.figure(figsize=(3, 1))
    f, (ax, axleg) = plt.subplots(2, figsize=(10, 10))
    for y, label in zip(opt_accuracies, opt_names):
        ax.plot(epochs, y, label=label)
    ax.set_ylabel('Test Accuracy')
    ax.set_xlabel('Epoch')
    axleg.axis('off')
    ax.legend()
    # plt.show()
    plt.savefig(output_path)


def get_accuracies(log_path, verbose=True):
    with open(log_path, "r") as f:
        logs = str(f.readlines())
    tests = logs.split('- Test Epoch')[1:]
    opt_accuracies = []
    opt_names = []
    epochs = []
    for t in tests:
        epoch = t.split('/')[0]
        epochs.append(int(epoch))
        optimizers = t.split('  - Loss:  ')[1:]
        if verbose:
            print('Epoch:', epoch)
        for i, o in enumerate(optimizers):
            name = o.split('\\n')[0].strip()
            acc = o.split('Accuracy(multiclass):')[1].split('\\n')[0].strip()
            if verbose:
                print(' ', name, 'accuracy:', acc)
            if len(opt_accuracies) < i + 1:
                opt_accuracies.append([])
                opt_names.append(name)
            opt_accuracies[i].append(float(acc))
    return epochs, opt_accuracies, opt_names


if __name__ == '__main__':
    main()
