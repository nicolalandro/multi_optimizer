import matplotlib.pyplot as plt

tr_name = {
    'artstate_weighted_1.0_0.0_momentum0.95_lr_0.001_change_cosineannealinglr_epochs350_batchsize_1024_mas_change_no_adam_w_end_0.0_1': 'ADAM',
    'artstate_weighted_0.0_1.0_momentum0.95_lr_0.001_change_cosineannealinglr_epochs350_batchsize_1024_mas_change_no_adam_w_end_0.0_2': 'SGD',
    'artstate_weighted_1.0_0.0_momentum0.95_lr_0.001_change_cosineannealinglr_epochs350_batchsize_1024_mas_change_step_adam_w_end_0.0_4': 'MAS'
}
file_names = [k for k, _ in tr_name.items()]
model = 'resnet18'
dataset = 'cifar10'
output_path = f'../../images/multi_optimizer/{dataset}/{model}/test_accuracies.pdf'


def main():
    fig = plt.figure()
    ax = plt.gca()

    for file_name in file_names:
        log_path = f'../../logs/multi_optimizer/{dataset}/{model}/{file_name}.log'
        print(log_path)

        epochs, opt_accuracies, opt_names = get_accuracies(log_path)

        for y, label in zip(opt_accuracies, opt_names):
            trans_file_name = tr_name[file_name]
            ax.plot(epochs, y, label=trans_file_name)
    ax.set_ylabel('Test Accuracy')
    ax.set_xlabel('Epoch')

    # To resize
    # ax.set_ylim(0.5, 0.95)
    # fig.set_figwidth(10)
    # fig.set_figheight(4)

    # axleg.axis('off')
    ax.legend()
    # plt.show()
    plt.savefig(output_path, bbox_inches='tight', pad_inches=0)


def get_accuracies(log_path, verbose=False):
    with open(log_path, "r") as f:
        logs = str(f.readlines())
    tests = logs.split('- Test Epoch')[1:]
    opt_accuracies = []
    opt_names = []
    epochs = []
    for t in tests:
        epoch = t.split('/')[0]
        epochs.append(int(epoch))
        optimizers = t.split('  - Optimizer  ')[1:]
        if verbose:
            print('Epoch:', epoch)
        for i, o in enumerate(optimizers):
            name = o.split('\\n')[0].strip()
            acc = o.split('Accuracy(multiclass):')[1].split('\\n')[0].strip()
            if verbose:
                print(' ', name, 'accuracy:', acc)
            if len(opt_accuracies) < i + 1:
                opt_accuracies.append([])
                opt_names.append(name)
            opt_accuracies[i].append(float(acc))
    return epochs, opt_accuracies, opt_names


if __name__ == '__main__':
    main()
