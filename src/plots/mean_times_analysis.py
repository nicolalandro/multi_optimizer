import os
from dateutil.parser import parse

# log_start_with = 'sgd'
# log_start_with = 'adam'
# log_start_with = 'weighted'
log_start_with = 'w_0.0_1.0'  # SGD
# log_start_with = 'w_1.0_0.0' # ADAM
# log_start_with = 'original'
# log_start_with = 'w_0.5_0.5'
model = 'BERT'
dataset = 'agnews'
log_path = f'../../logs/multi_optimizer/{dataset}/{model}/'


def main():
    log_files = os.listdir(log_path)
    log_files = list(filter(lambda x: x.startswith(log_start_with), log_files))
    all_epochs_time = []
    for f_name in log_files:
        f_path = os.path.join(log_path, f_name)
        epochs_time = get_epoch_times(f_path)
        print(f_name, sum(epochs_time) / len(epochs_time))
        all_epochs_time += epochs_time

    print('\nTotal avg', sum(all_epochs_time) / len(all_epochs_time))


def get_epoch_times(log_path):
    with open(log_path, "r") as f:
        logs = str(f.readlines())
    splits_train = logs.split('Finished. Total elapsed time (h:m:s):')[1:]

    epoch_times = [parse(s.split('\\n')[0]) for s in splits_train]
    all_epoch_sec = []

    start_time = parse('0:00:00')
    for e in zip(epoch_times):
        train_sec = (e[0] - start_time).total_seconds()
        all_epoch_sec.append(train_sec)
        start_time = e[0]

    return all_epoch_sec


if __name__ == '__main__':
    main()
