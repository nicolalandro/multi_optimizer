import os

from src.plots.multi_optimizer_accuracy_plot_from_file import get_accuracies
import matplotlib.pyplot as plt

log_start_with = 'sgd_momentum'
# log_start_with = 'adam'
# log_start_with = 'weighted_0.5_0.5'
# log_start_with = 'w_0.0_1.0'  # SGD
# log_start_with = 'w_1.0_0.0' # ADAM
# log_start_with = 'original'
# log_start_with = 'w_0.5_0.5'
model = 'resnet18'
dataset = 'cifar10'
# model = 'BERT'
# dataset = 'agnews'
log_path = f'../../logs/multi_optimizer/{dataset}/{model}/'
output_path = f'../../images/multi_optimizer/{dataset}/{model}/{log_start_with}_minmax.pdf'


def main():
    log_files = os.listdir(log_path)
    log_files = list(filter(lambda x: x.startswith(log_start_with), log_files))
    all_epochs_time = []
    for f_name in log_files:
        f_path = os.path.join(log_path, f_name)
        epochs, accuracies, _ = get_accuracies(f_path, verbose=False)
        accuracies = accuracies[0]
        print(f_name, sum(accuracies) / len(accuracies))
        all_epochs_time.append(accuracies)

    # print('\nTotal avg', sum(all_epochs_time) / len(all_epochs_time))
    ac_min = []
    ac_max = []
    ac_mean = []
    for acc in zip(*all_epochs_time):
        ac_min.append(min(acc))
        ac_max.append(max(acc))
        ac_mean.append(sum(acc) / len(acc))

    plt.figure(figsize=(3, 1))
    f, (ax, axleg) = plt.subplots(2, figsize=(10, 10))

    ax.plot(epochs, ac_min, label='Min')
    ax.plot(epochs, ac_max, label='Max')
    ax.plot(epochs, ac_mean, label='Avg')


    ax.set_ylabel('Test Accuracy')
    ax.set_xlabel('Epoch')
    axleg.axis('off')
    ax.legend()
    # plt.show()
    plt.savefig(output_path)


if __name__ == '__main__':
    main()
