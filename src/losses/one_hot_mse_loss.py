import torch


class OneHotMSE(torch.nn.Module):
    def __init__(self, use_cuda=True):
        super(OneHotMSE, self).__init__()
        self.use_cuda = use_cuda

    def forward(self, x, target):
        one_hot = torch.zeros(x.shape[0], x.shape[1])
        if self.use_cuda:
            one_hot = one_hot.cuda()
        one_hot_target = one_hot.scatter_(1, target.unsqueeze(dim=1), 1)
        softmax_input = torch.nn.functional.softmax(x, dim=1)
        mse = torch.nn.functional.mse_loss(softmax_input, one_hot_target)
        return mse


if __name__ == '__main__':
    target = torch.tensor([
        0,
        1
    ], dtype=torch.int64)
    pred = torch.Tensor([
        [8., 2.],
        [6.7, 7.]
    ])
    j = OneHotMSE(use_cuda=False)

    loss = j(pred, target)
    print(loss)
