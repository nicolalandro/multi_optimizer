import torch
EPOCHS = 2
LR = 0.1

# input
x = torch.tensor([1.])
# supervised output
y = torch.tensor([5.])

# random initialized weights
w = torch.tensor([1.], requires_grad=True)
b = torch.tensor([5.], requires_grad=True)

print('start w:', w.item(), ', start b:', b.item())

for epoch in range(EPOCHS):
    pred = w * x + b

    # mse
    diff = y - pred
    loss = diff * diff
    # to show the effect of something outside the graph
    # loss = diff * torch.tensor(diff.detach().numpy())

    # clean the gradient
    if w.grad is not None:
        w.grad.detach_()
        w.grad.zero_()
    if b.grad is not None:
        b.grad.detach_()
        b.grad.zero_()

    loss.backward()

    with torch.no_grad():
        w_grad = w.grad
        b_grad = b.grad
        w.add_(w_grad, alpha=-LR)
        b.add_(b_grad, alpha=-LR)

    print('EPOCH:', epoch, ', loss:', loss.item(), ', w:', w.item(), ', b:', b.item(), ', w grad:', w_grad.item(),
          ', b grad:', b_grad.item())
