import argparse
import copy
import datetime
import os
import time

import torch
import torch.nn as nn
import torchvision
from PIL import Image
from ignite.metrics import Accuracy
from torchvision import transforms

from src.losses.center_loss import CenterLoss
from src.losses.dice_loss import JaccardLoss
from src.losses.one_hot_mse_loss import OneHotMSE
from src.models.lenet5 import LeNet
from src.utils.average_meter import AverageMeter

parser = argparse.ArgumentParser("Train multiple loss")

# dataset
parser.add_argument('--path', type=str, default='/media/mint/Barracuda/Datasets/cifar10/')

# optimization
parser.add_argument('--batch-size', type=int, default=1024)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")

# model
parser.add_argument('--model', type=str, default='lenet5',
                    choices=['lenet5'])
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--when_save_model', type=int, default=500, help="save model at epoch x")
parser.add_argument('--save-dir', type=str,
                    default='/media/mint/Barracuda/Models/CIFAR_10/general')

# pythorch seed
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

# selected gpus
parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

parser.add_argument('--window', type=int, default='10', help='every range apoch is selected the best')

args = parser.parse_args()

BATCH_NUMBER_PRINT = 10


def main():
    print('Train Multi-Loss')
    torch.manual_seed(args.seed)

    # args.simple_model = 'resnet50_cifar100'
    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')

    test_gpu_settings()

    img_shape = (32, 32)
    transform_train = transforms.Compose([
        transforms.Resize(img_shape, Image.BILINEAR),
        # transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
    ])
    train_path = os.path.join(args.path, 'train')
    trainset = torchvision.datasets.ImageFolder(root=train_path, transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=2)

    transform_test = transforms.Compose([
        transforms.Resize(img_shape, Image.BILINEAR),
        transforms.ToTensor(),
    ])
    test_path = os.path.join(args.path, 'test')
    testset = torchvision.datasets.ImageFolder(root=test_path, transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)

    classes_dict = {i: v for i, v in enumerate(trainset.classes)}
    num_classes = len(classes_dict)

    features_size = 84
    if args.model == 'lenet5':
        orig_model = LeNet(num_classes, output_features=True, features_size=features_size)
    orig_model = nn.DataParallel(orig_model).cuda()

    criterion_xent = nn.CrossEntropyLoss().cuda()
    criterion_jaccard = JaccardLoss().cuda()
    criterion_one_hot_mse = OneHotMSE().cuda()
    criterion_center = CenterLoss(num_classes=num_classes, feat_dim=features_size).cuda()
    criterion_center_sum = CenterLoss(num_classes=num_classes, feat_dim=features_size).cuda()
    # losses_type = ['xent', 'center', 'center + xent']
    losses_type = ['xent', 'jaccard', 'one_hot_mse']
    # losses_type = ['one_hot_mse']

    params = list(orig_model.parameters())

    optimizer = torch.optim.Adam(params, args.lr)

    for i, o in enumerate(losses_type):
        print('Loss', i, ':')
        print(o)

    start_time = time.time()
    best_accuracies = [0 for _ in losses_type]
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))

        if epoch % args.window == 0:
            print('----clone orig_model')
            models = [LeNet(num_classes, output_features=True, features_size=features_size) for _ in losses_type]
            for i, m in enumerate(models):
                m = nn.DataParallel(m).cuda()
                m.load_state_dict(copy.deepcopy(orig_model.state_dict()))
                models[i] = m
        for model, loss_type in zip(models, losses_type):
            print('====> Loss: ', loss_type)
            optimizer.param_groups = []
            optimizer.add_param_group({'params': [p for p in model.parameters()]})
            if loss_type == 'xent':
                train_model(model, trainloader, criterion_xent, optimizer)
            elif loss_type == 'one_hot_mse':
                train_model(model, trainloader, criterion_jaccard, optimizer)
            elif loss_type == 'jaccard':
                train_model(model, trainloader, criterion_one_hot_mse, optimizer)
            elif loss_type == 'center':
                optimizer.add_param_group({'params': [p for p in criterion_center.parameters()]})
                train_model_features_loss(model, trainloader, criterion_center, optimizer)
            elif loss_type == 'center + xent':
                optimizer.add_param_group({'params': [p for p in criterion_center_sum.parameters()]})
                train_model_two_loss(model, trainloader, criterion_xent, criterion_center_sum, optimizer)
            else:
                print('!!!!!', loss_type, 'does not an option.')
                exit(1)

        # scheduler.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        print("- Test Epoch {}/{}".format(epoch + 1, args.max_epoch))
        for i, (model, loss_type) in enumerate(zip(models, losses_type)):
            print('  - Loss: ', loss_type)
            accuracy = test(model, testloader, criterion_xent)
            best_accuracies[i] += accuracy
            # best_accuracies[i] = accuracy

        if (epoch + 1) % args.window == 0:
            print('----set best weight to orig_model')
            mean_accuracies = [a / args.window for a in best_accuracies]
            max_value = max(mean_accuracies)
            max_index = mean_accuracies.index(max_value)
            print('  --winner:', losses_type[max_index], f'({max_index})')
            model = models[max_index]
            orig_model.load_state_dict(model.state_dict())
            best_accuracies = [0 for _ in losses_type]

        if (epoch + 1) % args.when_save_model == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model,
                       f'{args.save_dir}/{args.model}_{epoch + 1}.pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def train_model(model, trainloader, criterion_xent, optimizer):
    model.train()
    xent_losses = AverageMeter()

    for batch_idx, (data, labels) in enumerate(trainloader):
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        _, predictions = model(data)
        loss = criterion_xent(predictions, labels)

        optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        xent_losses.update(loss.item(), labels.size(0))

        if (batch_idx + 1) % BATCH_NUMBER_PRINT == 0:
            print(
                f"Batch {batch_idx + 1}\t CrossEntropy {xent_losses.val} ({xent_losses.avg})")


def train_model_features_loss(model, trainloader, criterion_center, optimizer):
    model.train()
    xent_losses = AverageMeter()

    for batch_idx, (data, labels) in enumerate(trainloader):
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        features, _ = model(data)
        loss = criterion_center(features, labels)

        optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        xent_losses.update(loss.item(), labels.size(0))

        if (batch_idx + 1) % BATCH_NUMBER_PRINT == 0:
            print(
                f"Batch {batch_idx + 1}\t Center Loss {xent_losses.val} ({xent_losses.avg})")


def train_model_two_loss(model, trainloader, criterion_xent, criterion_center, optimizer):
    model.train()
    sum_losses = AverageMeter()
    center_losses = AverageMeter()
    xent_losses = AverageMeter()

    for batch_idx, (data, labels) in enumerate(trainloader):
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        features, predictions = model(data)
        xent = criterion_xent(predictions, labels)
        center = criterion_center(features, labels)
        loss = xent + center

        optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        sum_losses.update(loss.item(), labels.size(0))
        xent_losses.update(xent.item(), labels.size(0))
        center_losses.update(center.item(), labels.size(0))

        if (batch_idx + 1) % BATCH_NUMBER_PRINT == 0:
            print(
                f"Batch {batch_idx + 1}\t "
                f"Total Loss {sum_losses.val} ({sum_losses.avg}), "
                f"CrossEntropy {xent_losses.val} ({xent_losses.avg}), "
                f"Center Loss {center_losses.val} ({center_losses.avg}), ")


def test(model, testloader, criterion_xent):
    model.eval()

    accuracy_metric = Accuracy()
    xent_losses = AverageMeter()

    with torch.no_grad():
        for data, labels in testloader:
            labels = labels.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            features, predictions = model(data)

            loss = criterion_xent(predictions, labels)
            xent_losses.update(loss.item(), labels.size(0))

            accuracy_metric.update((predictions, labels))

    test_crossentropy_loss = xent_losses.avg
    accuracy = accuracy_metric.compute()
    print(
        f'    Test Crossentropy: {test_crossentropy_loss}, Accuracy({accuracy_metric._type}): {accuracy}')
    return accuracy


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
    else:
        print("Currently using CPU")
        exit()


if __name__ == '__main__':
    main()
