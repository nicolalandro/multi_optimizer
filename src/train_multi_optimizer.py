import argparse
import copy
import datetime
import os
import time

import torch
import torch.nn as nn
from torch.optim.lr_scheduler import StepLR, CosineAnnealingLR
import torchvision
from PIL import Image
from ignite.metrics import Accuracy
from torchvision import transforms

try:
    import src
except:
    import sys

    sys.path.insert(0, './')

from src.models import cifar_resnet
from src.models.lenet5 import LeNet
from src.optimizers.adam_sgd_mix import AdamSGDWeighted
from src.utils.average_meter import AverageMeter
from src.optimizers.mas_scheduler import MASScheduler


parser = argparse.ArgumentParser("Train multiple optimizer")

# dataset
parser.add_argument('--path', type=str, default='/media/mint/Barracuda/Datasets/cifar10/')

# optimization
parser.add_argument('--batch-size', type=int, default=1024)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for optimizer")
parser.add_argument('--lr-change', type=str, default='no', choices=['no', 'step', 'cosineannealinglr'],
                    help="decrease lr at epoch 150")
parser.add_argument('--weight-decay', type=float, default=0, help="weight decay")
parser.add_argument('--momentum', type=float, default=0.95, help="sgd momentum")
parser.add_argument('--adam-w', type=float, default=0.3, help="adam weight")
parser.add_argument('--sgd-w', type=float, default=0.7, help="sgd weight")

parser.add_argument('--mas-change', type=str, default='no', choices=['no', 'step'],
                    help="decrease adam_w from --adam-w value to --adam-max-w and sgd_w as 1-adma_w")
parser.add_argument('--adam-end-w', type=float, default=0.0, help="adam weight end after all steps")

# model
parser.add_argument('--model', type=str, default='resnet18',
                    choices=['lenet5', 'resnet18', 'resnet34'])
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--save-freq', type=int, default=20000, help="save model at epoch x")
parser.add_argument('--save-dir', type=str,
                    default='/media/mint/Barracuda/Models/CIFAR_10/general')

# pythorch seed
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

# selected gpus
parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

parser.add_argument('--window', type=int, default=20000, help='every range apoch is selected the best')

args = parser.parse_args()


def main():
    print('Train Multi-Optimizer')
    torch.manual_seed(args.seed)

    # args.simple_model = 'resnet50_cifar100'
    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')

    test_gpu_settings()

    img_shape = (32, 32)
    transform_train = transforms.Compose([
        transforms.Resize(img_shape, Image.BILINEAR),
        transforms.RandomCrop(32, padding=4),
        transforms.RandomHorizontalFlip(),
        # transforms.RandomRotation(90),
        transforms.ToTensor(),
    ])
    train_path = os.path.join(args.path, 'train')
    trainset = torchvision.datasets.ImageFolder(root=train_path, transform=transform_train)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=2)

    transform_test = transforms.Compose([
        transforms.Resize(img_shape, Image.BILINEAR),
        transforms.ToTensor(),
    ])
    test_path = os.path.join(args.path, 'test')
    if not os.path.exists(test_path):
        test_path = os.path.join(args.path, 'val')
    testset = torchvision.datasets.ImageFolder(root=test_path, transform=transform_test)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)

    classes_dict = {i: v for i, v in enumerate(trainset.classes)}
    num_classes = len(classes_dict)

    features_size = 2048

    def create_model():
        if args.model == 'lenet5':
            model = LeNet(num_classes, features_size=features_size)
        elif args.model == 'resnet18':
            model = cifar_resnet.ResNet18(num_classes=num_classes)
        elif args.model == 'resnet34':
            model = cifar_resnet.ResNet34(num_classes=num_classes)
        return model

    orig_model = create_model()
    orig_model = nn.DataParallel(orig_model).cuda()
    criterion_xent = nn.CrossEntropyLoss().cuda()

    params = list(orig_model.parameters())

    proposed_optimizer = AdamSGDWeighted(params, args.lr, momentum=args.momentum, adam_w=args.adam_w, sgd_w=args.sgd_w,
                                         weight_decay=args.weight_decay)

    # optimizer1 = torch.optim.SGD(params, 0.001, momentum=0.95)
    # optimizer2 = torch.optim.Adam(params, 0.001)
    # optimizer3 = torch.optim.RMSprop(params, lr=args.lr, alpha=0.99, eps=1e-8, momentum=0, centered=False)
    # optimizer3 = torch.optim.Adadelta(params, )

    # optimizer1 = torch.optim.SGD(params, 0.01, momentum=0.95)
    # optimizer2 = torch.optim.SGD(params, 0.01)

    # optimizer1 = torch.optim.Adam(params, 0.001)
    # optimizer2 = torch.optim.Adam(params, 0.0001)

    # optimizers = [optimizer3]
    # optimizers = [optimizer1, optimizer2]
    # optimizers = [optimizer1, optimizer2, optimizer3]
    optimizers = [proposed_optimizer]

    if args.lr_change == 'step':
        scheduler = StepLR(proposed_optimizer, step_size=150, gamma=0.1, last_epoch=-1)
    elif args.lr_change == 'cosineannealinglr':
        scheduler = CosineAnnealingLR(proposed_optimizer, args.max_epoch)

    if args.mas_change == 'step':
        mas_scheduler = MASScheduler(proposed_optimizer, args.adam_w,
                                     args.adam_end_w, args.max_epoch)

    for i, o in enumerate(optimizers):
        print('Optimizer', i, ':')
        print(o)

    start_time = time.time()
    best_accuracies = [0 for _ in optimizers]
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))

        if epoch % args.window == 0:
            print('----clone orig_model')
            models = [create_model() for _ in optimizers]
            for i, m in enumerate(models):
                m = nn.DataParallel(m).cuda()
                m.load_state_dict(copy.deepcopy(orig_model.state_dict()))
                models[i] = m
        for model, optimizer in zip(models, optimizers):
            print('====> Optimizer ', type(optimizer))
            optimizer.param_groups = []
            optimizer.add_param_group({'params': [p for p in model.parameters()]})
            train_model(model, trainloader, criterion_xent, optimizer)
        if args.lr_change != 'no':
            scheduler.step()
        if args.mas_change != 'no':
            mas_scheduler.step()

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        print("- Test Epoch {}/{}".format(epoch + 1, args.max_epoch))
        for i, (model, optimizer) in enumerate(zip(models, optimizers)):
            print('  - Optimizer ', type(optimizer))
            accuracy = test(model, testloader, criterion_xent)
            best_accuracies[i] += accuracy

        if (epoch + 1) % args.window == 0:
            print('----set best weight to orig_model')
            mean_accuracies = [a / args.window for a in best_accuracies]
            max_value = max(mean_accuracies)
            max_index = mean_accuracies.index(max_value)
            print('  --winner:', type(optimizers[max_index]), f'({max_index})')
            model = models[max_index]
            orig_model.load_state_dict(model.state_dict())
            best_accuracies = [0 for _ in optimizers]

        if (epoch + 1) % args.save_freq == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model,
                       f'{args.save_dir}/{args.model}_{epoch + 1}.pt')
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def train_model(model, trainloader, criterion_xent, optimizer):
    model.train()
    xent_losses = AverageMeter()

    for batch_idx, (data, labels) in enumerate(trainloader):
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        predictions = model(data)
        loss = criterion_xent(predictions, labels)

        optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        xent_losses.update(loss.item(), labels.size(0))

        if (batch_idx + 1) % 10 == 0:
            print(
                f"Batch {batch_idx + 1}\t CrossEntropy {xent_losses.val} ({xent_losses.avg})")


def test(model, testloader, criterion_xent):
    model.eval()

    accuracy_metric = Accuracy()
    xent_losses = AverageMeter()

    with torch.no_grad():
        for data, labels in testloader:
            labels = labels.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            predictions = model(data)

            loss = criterion_xent(predictions, labels)
            xent_losses.update(loss.item(), labels.size(0))

            accuracy_metric.update((predictions, labels))

    test_crossentropy_loss = xent_losses.avg
    accuracy = accuracy_metric.compute()
    print(
        f'    Test Crossentropy: {test_crossentropy_loss}, Accuracy({accuracy_metric._type}): {accuracy}')
    return accuracy


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
    else:
        print("Currently using CPU")
        exit()


if __name__ == '__main__':
    main()
