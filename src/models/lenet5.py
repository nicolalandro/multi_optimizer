import torch
from torch import nn


class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)


class LeNet(nn.Module):
    def __init__(self, num_classes, features_size=84, output_features=False):
        super(LeNet, self).__init__()
        conv_1_in_ch = 3
        conv_1_out_ch = 6
        conv_1_kernel_size = 5
        self.conv1 = nn.Conv2d(in_channels=conv_1_in_ch, out_channels=conv_1_out_ch, kernel_size=conv_1_kernel_size)
        pool_1_kernel_size = 2
        pool_1_stride = 2
        self.relu_cov_1 = nn.ReLU()
        self.pool1 = nn.MaxPool2d(kernel_size=pool_1_kernel_size, stride=pool_1_stride)
        conv_2_out_ch = 16
        conv_2_kernel_size = 5
        self.conv2 = nn.Conv2d(in_channels=conv_1_out_ch, out_channels=conv_2_out_ch, kernel_size=conv_2_kernel_size)
        self.relu_cov_2 = nn.ReLU()
        pool_2_kernel_size = 2
        pool_2_stride = 2
        self.pool2 = nn.MaxPool2d(kernel_size=pool_2_kernel_size, stride=pool_2_stride)
        self.flatten = Flatten()
        intermediate_size = 120
        self.dense1 = nn.Linear(conv_2_out_ch * conv_2_kernel_size * conv_1_kernel_size, intermediate_size)
        self.relu_dense_1 = nn.ReLU()
        self.dense2 = nn.Linear(intermediate_size, features_size)
        self.relu_dense_2 = nn.ReLU()
        self.classifier = nn.Linear(features_size, num_classes)
        self.output_features = output_features

    def forward(self, x):
        conv_1 = self.conv1(x)
        relu_cov_1 = self.relu_cov_1(conv_1)
        pool_1 = self.pool1(relu_cov_1)
        conv_2 = self.conv2(pool_1)
        relu_conv_2 = self.relu_cov_2(conv_2)
        pool_2 = self.pool2(relu_conv_2)
        flatten = self.flatten(pool_2)
        dense_1 = self.dense1(flatten)
        relu_dense_1 = self.relu_dense_1(dense_1)
        dense_2 = self.dense2(relu_dense_1)
        relu_dense_2 = self.relu_dense_2(dense_2)
        dense_3 = self.classifier(relu_dense_2)
        if self.output_features:
            return dense_2, dense_3
        return dense_3


if __name__ == '__main__':
    model = LeNet(num_classes=10)
    x = torch.zeros(1, 3, 32, 32, dtype=torch.float, requires_grad=False)
    output = model(x)
    print(output.shape)
    assert list(output.shape) == [1, 10]
