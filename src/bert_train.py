import argparse
import datetime
import os
import time

import pandas as pd
import torch
from ignite.metrics import Accuracy
from torch.utils.data import TensorDataset, RandomSampler, DataLoader
from tqdm import tqdm
from transformers import BertTokenizer, BertForSequenceClassification, AdamW, get_linear_schedule_with_warmup

try:
    import src
except:
    import sys

    sys.path.insert(0, './')

from src.optimizers.adam_sgd_mix import AdamSGDWeighted
from src.utils.average_meter import AverageMeter
from src.optimizers.mas_scheduler import MASScheduler

parser = argparse.ArgumentParser("BERT")
parser.add_argument('--path', type=str, default='/media/mint/Barracuda/Datasets/AGnews')
parser.add_argument('--dataset', type=str, default='agnews', choices=['cola', 'agnews'])
parser.add_argument('--test-file', type=str, default='/media/mint/Barracuda/Datasets/cifar10/')

parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')
parser.add_argument('--seed', type=int, default=1, help='using a fixed seed to be reproducible')

parser.add_argument('--seq-max-len', type=int, default=64, help='sequence max length ex. 64 for Cola')
parser.add_argument('--batch-size', type=int, default=100)
parser.add_argument('--lr', type=float, default=2e-5, help="learning rate for optimizer")
parser.add_argument('--momentum', type=float, default=0.95, help="sgd momentum")
parser.add_argument('--adam-w', type=float, default=0.5, help="adam weight")
parser.add_argument('--sgd-w', type=float, default=0.5, help="sgd weight")
parser.add_argument('--max-epoch', type=int, default=50)
parser.add_argument('--original', type=int, default=0)

parser.add_argument('--mas-change', type=str, default='no', choices=['no', 'step'],
                    help="decrease adam_w from --adam-w value to --adam-max-w and sgd_w as 1-adma_w")
parser.add_argument('--adam-end-w', type=float, default=0.0, help="adam weight end after all steps")

args = parser.parse_args()

os.environ['CUDA_LAUNCH_BLOCKING'] = '1'


def main():
    print('Train Multi-Optimizer')
    torch.manual_seed(args.seed)

    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')

    device = test_gpu_settings()

    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True)

    if args.dataset == 'cola':
        train_path = os.path.join(args.path, 'in_domain_train.tsv')
        train_labels, train_sentences = read_CoLA_file(train_path)
        num_labels = 2
    elif args.dataset == 'agnews':
        train_path = os.path.join(args.path, 'train.csv')
        train_labels, train_sentences = read_agnews_file(train_path)
        num_labels = 4
    train_attention_masks, train_input_ids, train_labels = get_data_for_dataloader(train_labels, train_sentences,
                                                                                   tokenizer)

    train_dataset = TensorDataset(train_input_ids, train_attention_masks, train_labels)
    train_dataloader = DataLoader(
        train_dataset,
        sampler=RandomSampler(train_dataset),
        batch_size=args.batch_size
    )

    if args.dataset == 'cola':
        test_path = os.path.join(args.path, 'in_domain_dev.tsv')
        test_labels, test_sentences = read_CoLA_file(test_path)
    elif args.dataset == 'agnews':
        test_path = os.path.join(args.path, 'test.csv')
        test_labels, test_sentences = read_agnews_file(test_path)
    test_attention_masks, test_input_ids, test_labels = get_data_for_dataloader(test_labels, test_sentences, tokenizer)
    test_dataset = TensorDataset(test_input_ids, test_attention_masks, test_labels)
    test_dataloader = DataLoader(
        test_dataset,
        sampler=RandomSampler(test_dataset),
        batch_size=args.batch_size
    )

    print('creating model...')
    model = BertForSequenceClassification.from_pretrained(
        "bert-base-uncased",
        num_labels=num_labels,
        output_attentions=False,
        output_hidden_states=False,
    )
    if device == 'cuda':
        model = torch.nn.DataParallel(model)
    model = model.to(device)

    if args.original == 1:
        optimizer = AdamW(model.parameters(), lr=args.lr, eps=1e-8)
    else:
        optimizer = AdamSGDWeighted(model.parameters(), lr=args.lr, momentum=args.momentum, adam_w=args.adam_w,
                                    sgd_w=args.sgd_w)
    print(optimizer)

    total_steps = len(train_dataloader) * args.max_epoch
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)

    if args.mas_change == 'step':
        mas_scheduler = MASScheduler(optimizer, args.adam_w,
                                     args.adam_end_w, args.max_epoch)

    start_time = time.time()
    for epoch in range(0, args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))
        model.train()
        losses = AverageMeter()
        for batch_idx, (b_input_ids, b_input_mask, b_labels) in enumerate(train_dataloader):
            b_input_ids = b_input_ids.to(device)
            b_input_mask = b_input_mask.to(device)
            b_labels = b_labels.to(device)

            model.zero_grad()
            loss, logits = model(b_input_ids,
                                 token_type_ids=None,
                                 attention_mask=b_input_mask,
                                 labels=b_labels)
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
            optimizer.step()
            scheduler.step()
            if args.mas_change != 'no':
                mas_scheduler.step()

            losses.update(loss.item(), b_labels.size(0))
            if (batch_idx + 1) % 10 == 0:
                print(f"Batch {batch_idx + 1}\t Loss {losses.val} ({losses.avg})")

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))

        print("Test Epoch {}/{}".format(epoch + 1, args.max_epoch))
        model.eval()
        accuracy_metric = Accuracy()
        losses = AverageMeter()
        with torch.no_grad():
            for batch_idx, (b_input_ids, b_input_mask, b_labels) in enumerate(test_dataloader):
                b_input_ids = b_input_ids.to(device)
                b_input_mask = b_input_mask.to(device)
                b_labels = b_labels.to(device)

                loss, logits = model(b_input_ids,
                                     token_type_ids=None,
                                     attention_mask=b_input_mask,
                                     labels=b_labels)

                losses.update(loss.item(), b_labels.size(0))
                accuracy_metric.update((logits, b_labels))

        accuracy = accuracy_metric.compute()
        print(
            f'    Test Loss: {losses.avg}, Accuracy({accuracy_metric._type}): {accuracy}')


def read_CoLA_file(path):
    df_test = pd.read_csv(
        path, delimiter='\t', header=None,
        names=['sentence_source', 'label', 'label_notes', 'sentence']
    )
    sentences = df_test.sentence.values
    labels = df_test.label.values
    return labels, sentences


def read_agnews_file(path):
    df_test = pd.read_csv(
        path, delimiter=',', header=None,
        names=['label', 'title', 'sentence']
    )
    sentences = df_test.sentence.values
    labels = df_test.label.values - 1
    # unique_labels = set(labels)
    # print('Label size:', len(unique_labels), unique_labels)
    return labels, sentences


def get_data_for_dataloader(labels, sentences, tokenizer):
    input_ids = []
    attention_masks = []
    for sent in tqdm(sentences):
        encoded_dict = tokenizer.encode_plus(
            sent,  # Sentence to encode.
            add_special_tokens=True,  # Add '[CLS]' and '[SEP]'
            max_length=args.seq_max_len,  # Pad & truncate all sentences.
            padding="max_length",
            return_attention_mask=True,  # Construct attn. masks.
            return_tensors='pt',  # Return pytorch tensors.,
            truncation=True,
        )
        # print(encoded_dict['input_ids'].shape, encoded_dict['attention_mask'].shape)
        # Add the encoded sentence to the list.
        input_ids.append(encoded_dict['input_ids'])

        # And its attention mask (simply differentiates padding from non-padding).
        attention_masks.append(encoded_dict['attention_mask'])

        # print(encoded_dict['input_ids'].shape, encoded_dict['attention_mask'].shape)
    # Convert the lists into tensors.
    input_ids = torch.cat(input_ids, dim=0)
    attention_masks = torch.cat(attention_masks, dim=0)
    labels = torch.tensor(labels)
    print(input_ids.shape, attention_masks.shape, labels.shape)
    return attention_masks, input_ids, labels


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    # sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
    #     args.percentage_used) + '.txt'))
    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        print(f"Learning rate optimizer {args.lr}  Batch-size {args.batch_size}")
        return 'cuda'
    else:
        print("Currently using CPU")
        return 'cpu'


if __name__ == '__main__':
    main()
