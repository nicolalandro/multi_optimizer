#!/usr/bin/env bash

DATASET_PATH="/home/super/datasets-nas/cifar10"
DATASET_NAME='cifar10'

GPU="0"
MODEL="resnet34"
BATCH_SIZE="512"

LR=0.001
LR_CHANGE="cosineannealinglr"
MOMENTUM=0.95
EPOCHS=350
WEIGHT_DECAY="5e-4"
MAS_CHANGE="no"
ADAM_END_W=0.0


mkdir -p "logs/new_multi_optimizer/${DATASET_NAME}/${MODEL}"


MAS_CHANGE="no"
ADAM_W=0.0
SGD_W=1.0
for RUN_NUMBER in 5 6
do
  python3 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr="${LR}" --batch-size="$BATCH_SIZE" \
          --momentum="$MOMENTUM" --adam-w="$ADAM_W" --sgd-w="$SGD_W" --gpu="$GPU" --lr-change="$LR_CHANGE"\
          --max-epoch="$EPOCHS" --weight-decay="$WEIGHT_DECAY" --seed="$RUN_NUMBER"\
          --mas-change="${MAS_CHANGE}" --adam-end-w="${ADAM_END_W}" \
          --model="$MODEL" > "logs/new_multi_optimizer/${DATASET_NAME}/${MODEL}/artstate_weighted_${ADAM_W}_${SGD_W}_momentum${MOMENTUM}_lr_${LR}_change_${LR_CHANGE}_epochs${EPOCHS}_batchsize_${BATCH_SIZE}_mas_change_${MAS_CHANGE}_adam_w_end_${ADAM_END_W}_${RUN_NUMBER}.log"
done