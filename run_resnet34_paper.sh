#!/usr/bin/env bash

#DATASET_PATH="/home/risen/datasets-nas/cifar100"
DATASET_PATH="/media/mint/Barracuda/Datasets/cifar10"
DATASET_NAME='cifar10'
GPU="0"
MODEL="resnet34"
BATCH_SIZE="512"
RUN_NUMBER=0
EPOCHS=200
LR=1.0
WEIGHT_DECAY=0.0005
LR_CHANGE='yes'

mkdir -p "logs/multi_optimizer/${DATASET_NAME}/${MODEL}_paper"

ADAM_W=0.0
SGD_W=1.0
python3.6 src/train_multi_optimizer.py --path="$DATASET_PATH" --lr="$LR" --batch-size="$BATCH_SIZE" \
          --momentum=0.95 --adam-w="$ADAM_W" --sgd-w="$SGD_W" --gpu="$GPU" --model="$MODEL" --max-epoch="$EPOCHS" \
          --weight-decay="$WEIGHT_DECAY" --lr-change="$LR_CHANGE" \
          > "logs/multi_optimizer/${DATASET_NAME}/${MODEL}_paper/weighted_${ADAM_W}_${SGD_W}_lr_${LR}_weight_decay_${WEIGHT_DECAY}_${RUN_NUMBER}.log"